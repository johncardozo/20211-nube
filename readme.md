# Repositorio de Nube

_Profesor:_ John Cardozo

## Tecnologías

- HTML
- CSS
- Django

## Lenguajes de programación

- Python
- Javascript

## Herramientas

- Visual Studio Code
- iTerm 2
- Git
- AWS
