from django.urls import path
from . import views

app_name = 'app'
urlpatterns = [
    path('', views.index, name='index'),

    path('registro/', views.form_registro, name='form_registro'),
    path('registro_post/', views.registro, name='registro'),

    path('login/', views.form_login, name='form_login'),
    path('iniciar_sesion/', views.iniciar_sesion, name="iniciar_sesion"),
    path('logout/', views.cerrar_sesion, name='cerrar_sesion'),

    path('peliculas/', views.peliculas, name='peliculas'),
    path('peliculas/<int:id>/', views.pelicula, name='pelicula'),
    path('peliculas/crear/', views.form_crear_pelicula,
         name='form_crear_pelicula'),
    path('peliculas/crear_post/',
         views.crear_pelicula_post, name='crear_pelicula'),
    path('peliculas/eliminar/<int:id>/', views.eliminar_confirmacion,
         name="eliminar_confirmacion"),
    path('peliculas/eliminar_post/<int:id>/', views.eliminar_pelicula_post,
         name='eliminar_pelicula_post'),

    path('categorias/', views.categorias, name='categorias'),
    path('categorias/<int:id>/', views.categoria_view, name='categoria'),
    path('categorias/crear/', views.form_crear_categoria,
         name='form_crear_categoria'),
    path('categorias/crear_post/',
         views.crear_categoria_post, name='crear_categoria'),

    path('acerca/', views.acerca, name='acerca'),
]
