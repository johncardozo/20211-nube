from django.db import models


class Categoria(models.Model):
    nombre = models.CharField(max_length=100, null=False)
    descripcion = models.TextField(null=True)

    def __str__(self):
        return self.nombre

    class Meta:
        app_label = 'app'


class Director(models.Model):
    nombres = models.CharField(max_length=100, null=True)
    apellidos = models.CharField(max_length=100, null=True)
    nacionalidad = models.CharField(max_length=100, null=True)

    def __str__(self):
        return f'{self.nombres} {self.apellidos}'

    class Meta:
        app_label = 'app'


class Pelicula(models.Model):
    titulo = models.CharField(max_length=300, null=False)
    year = models.IntegerField(null=True)
    sinopsis = models.TextField(null=True)
    categoria = models.ForeignKey(
        Categoria,
        related_name='peliculas',
        null=False,
        on_delete=models.PROTECT
    )
    director = models.ForeignKey(
        Director,
        related_name='peliculas',
        null=True,
        on_delete=models.PROTECT
    )
    fecha_creacion = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.titulo

    class Meta:
        app_label = 'app'
