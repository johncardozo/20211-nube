from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.shortcuts import get_object_or_404


from app.models import Categoria, Pelicula


def index(request):
    # return HttpResponse("Bienvenidos a BLOCKBUSTER")
    return render(request, 'app/index.html')


def peliculas(request):
    # Obtiene la lista de peliculas
    lista_peliculas = Pelicula.objects.all()
    # crear el contexto
    contexto = {
        'peliculas': lista_peliculas
    }
    return render(request, 'app/peliculas.html', contexto)


def pelicula(request, id):
    # Obtiene la pelicula
    pelicula = get_object_or_404(Pelicula, pk=id)
    contexto = {
        'pelicula': pelicula
    }
    return render(request, 'app/pelicula.html', contexto)


def acerca(request):
    return render(request, 'app/acerca.html')


def categorias(request):
    # Obtiene la lista de categorias
    lista_categorias = Categoria.objects.all()
    # Crea el contexto
    contexto = {
        'categorias': lista_categorias
    }
    return render(request, 'app/categorias.html', contexto)


def categoria_view(request, id):

    # Obtiene la categoria, y si no existe retorna erro 404
    categoria = get_object_or_404(Categoria, pk=id)

    # Obtiene la categoria
    #categoria = Categoria.objects.get(id=id)

    # Crea el contexto
    contexto = {
        'categoria': categoria
    }

    return render(request, 'app/categoria.html', contexto)


@login_required
def form_crear_categoria(request):
    return render(request, 'app/formCrearCategoria.html')


def crear_categoria_post(request):
    # Obtiene los datos de la categoría
    nombre = request.POST['nombre']
    descripcion = request.POST['descripcion']
    # Crea la categoría
    categoria = Categoria()
    categoria.nombre = nombre
    categoria.descripcion = descripcion
    # Guarda la categoria en la base de datos
    categoria.save()
    # Redirecciona a la página de categorías
    return redirect('app:categorias')


@login_required
def form_crear_pelicula(request):
    # Obtiene las categorías
    categorias = Categoria.objects.all()
    # Crea el contexto
    contexto = {
        'categorias': categorias
    }
    return render(request, 'app/formCrearPelicula.html', contexto)


def crear_pelicula_post(request):
    # Obtiene la información del formulario
    titulo = request.POST['titulo']
    id_categoria = int(request.POST['categoria'])
    year = int(request.POST['year'])

    # Obtiene la categoria
    categoria = Categoria.objects.get(id=id_categoria)

    # Crea la pelicula
    pelicula = Pelicula()
    pelicula.titulo = titulo
    pelicula.year = year
    pelicula.categoria = categoria

    # Guarda la pelicula en la BD
    pelicula.save()

    return redirect('app:peliculas')


def eliminar_confirmacion(request, id):
    # Obtiene la pelicula
    pelicula = Pelicula.objects.get(id=id)
    # Crea el contexto
    contexto = {
        'pelicula': pelicula
    }
    return render(request, 'app/eliminarPelicula.html', contexto)


def eliminar_pelicula_post(request, id):
    # Obtiene la pelicula por su id
    pelicula = Pelicula.objects.get(id=id)
    # Elimina la película
    pelicula.delete()
    return redirect('app:peliculas')


def form_registro(request):
    return render(request, 'app/registro.html')


def registro(request):
    # Obtiene los datos digitados en el formulario
    username = request.POST['username']
    email = request.POST['email']
    password = request.POST['password']

    # Crea el usuario
    usuario = User()
    usuario.username = username
    usuario.email = email
    usuario.set_password(password)

    # Guarda el usuario en la BD
    usuario.save()

    return redirect('app:index')


def form_login(request):
    return render(request, 'app/login.html')


def iniciar_sesion(request):
    # Obtiene los datops digitados por el usuario
    u = request.POST['username']
    p = request.POST['password']

    # Obtiene el usuario con el username y password
    usuario = authenticate(username=u, password=p)

    # Verifica que el usuario sea válido
    if usuario is not None:
        # Inicia la sesión del usuario
        login(request, usuario)
        # redirecciona al index
        return redirect('app:index')
    else:
        contexto = {
            'error': 'Credenciales no válidas'
        }
        return render(request, 'app/login.html', contexto)


def cerrar_sesion(request):
    # Cierra la sesión del usuario
    logout(request)

    # Redireccion al login
    return redirect('app:form_login')
